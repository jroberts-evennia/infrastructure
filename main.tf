provider "aws" {
    region = "eu-west-1"

    assume_role {
        role_arn = "arn:aws:iam::792400354936:role/OrganizationAccountAccessRole"
    }
}

terraform {
  backend "s3" {
    bucket = "evennia-tfstate"
    region = "eu-west-1"
    key    = "evennia.tfstate"
    role_arn = "arn:aws:iam::792400354936:role/OrganizationAccountAccessRole"
  }
}
